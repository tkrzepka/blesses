//
// Created by Tomasz Rzepka on 13/06/15.
//

#include "StateMachine.h"
#include "../Controller.h"
#include "../Settings.h"
//#include "State.h"
#define RESIZE_KEY 0x19a
#define ESC_KEY 0x01b
#define TAB_KEY 0x009

void Blesses::StateMachine::sendInput(int p_key) {
    switch (p_key) {
        case ERR:
            break;
        case ESC_KEY:
            Controller::stop();
            break;
        case RESIZE_KEY:
            resize();
        default:
            if(m_state->sendInput(p_key))
                pop();
    }
}

void Blesses::StateMachine::resize() {
    if(m_state != m_resize){
        m_stateStack.push(m_state);
        m_state = m_resize;
    }
}

void Blesses::StateMachine::draw(bool p_clean) {
    if(!m_state) {
        Controller::stop();
        return;
    }

    m_state->clean();
    m_state->draw();
    refresh();
}

void Blesses::StateMachine::drawAll() {
    m_stateStack.draw();
    draw(false);
}

void Blesses::StateMachine::pop() {
    erase();
    if(!m_stateStack.empty()){
        m_state = m_stateStack.top();
        m_stateStack.pop();
    }else
        Controller::stop();
}

void Blesses::StateMachine::setState(std::shared_ptr<State>& p_state) {
    if(m_state)
        m_stateStack.push(m_state);
    m_state = p_state;
}

void Blesses::StateMachine::initialize() {
    m_resize = std::make_shared<Resize>();
    int l_width, l_height;
    getmaxyx(stdscr, l_height, l_width);
    if(l_height<Settings::getSettings()->getMinHeight()
       || l_width<Settings::getSettings()->getMinWidth())
        setState(m_resize);
}

void Blesses::StateMachine::pushState(std::shared_ptr<State> &p_state) {
    erase();
    if(m_state)
        m_stateStack.push(m_state);
    m_state = p_state;
}

void Blesses::StateMachine::acceptVisitor(std::shared_ptr<Visitor> p_visitor) {
    m_stateStack.acceptVisitor(p_visitor);
    p_visitor->visit(m_state);
}
