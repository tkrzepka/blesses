//
// Created by Tomasz Rzepka on 13/06/15.
//

#pragma once

#include "Resize.h"
#include "StateStack.h"
#include "Visitor.h"
#include <memory>
#include <deque>
#include <ncurses.h>

class IState;

namespace Blesses {
    class StateMachine {
    public:
        void sendInput(int p_key);
        void draw(bool p_clean = true);
        void drawAll();
        void resize();
        void pop();
        void initialize();
        void pushState(std::shared_ptr<State> &p_state);
        void setState(std::shared_ptr<State>& p_state);
        void acceptVisitor(std::shared_ptr<Visitor> p_visitor);
    private:
        std::shared_ptr<State> m_resize;
        std::shared_ptr<State> m_state;
        StateStack m_stateStack{};
    };
}
