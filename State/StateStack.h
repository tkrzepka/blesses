//
// Created by Tomasz Rzepka on 14/06/15.
//

#pragma once
#include <memory>
#include <deque>
#include "State.h"
#include "Visitor.h"

namespace Blesses {
    class StateStack {
    public:
        bool empty() const;
        std::shared_ptr<State> top() const;
        void pop();
        void push(std::shared_ptr<State> &p_state);
        void draw();
        void acceptVisitor(std::shared_ptr<Visitor>& p_visitor);

    private:
        std::deque<std::shared_ptr<State>> m_stateStack;
    };
};
