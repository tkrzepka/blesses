//
// Created by Tomasz Rzepka on 13/06/15.
//

#pragma once
#include <string>
#include <memory>
#include <ncurses.h>
#include "Decorator.h"

namespace Blesses {
    class IStateFactory;

    class State {
    public:
        virtual std::string getName() = 0;
        virtual bool sendInput(int p_key) = 0;
        virtual void clean() = 0;
        virtual void draw(){
            if(m_decorator)
                m_decorator->decorate(m_win);
        };

    protected:
        std::shared_ptr<IStateFactory> m_stateFactory;
        std::shared_ptr<Decorator> m_decorator;
        WINDOW *m_win{nullptr};
        int m_width;
        int m_height;
    };
}
