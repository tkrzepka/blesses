//
// Created by Tomasz Rzepka on 14/06/15.
//

#include "StateStack.h"
#include <boost/range/adaptor/reversed.hpp>
bool Blesses::StateStack::empty() const {
    return m_stateStack.empty();
}

std::shared_ptr<Blesses::State> Blesses::StateStack::top() const {
    return m_stateStack.front();
}

void Blesses::StateStack::pop() {
    m_stateStack.pop_front();
}

void Blesses::StateStack::push(std::shared_ptr<Blesses::State>& p_state) {
    m_stateStack.push_front(p_state);
}

void Blesses::StateStack::draw() {
    for(auto it = m_stateStack.rbegin(); it != m_stateStack.rend(); it++){
        (*it)->clean();
        (*it)->draw();
    }
}

void Blesses::StateStack::acceptVisitor(std::shared_ptr<Visitor> &p_visitor) {
    for(auto it = m_stateStack.rbegin(); it != m_stateStack.rend(); it++){
        p_visitor->visit(*it);
    }
}
