//
// Created by Tomasz Rzepka on 14/06/15.
//

#pragma once
#include <memory>
#include "State.h"

namespace Blesses {
    class IStateFactory {
    public:
        virtual std::shared_ptr<State> createState(int i) = 0;
    };
}