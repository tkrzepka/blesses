//
// Created by Tomasz Rzepka on 14/06/15.
//

#pragma once
#include <memory>
#include "State.h"

namespace Blesses {
    class Visitor {
    public:
        virtual void visit(std::shared_ptr<State> &p_state) = 0;
    };
}