//
// Created by Tomasz Rzepka on 13/06/15.
//

#include "Resize.h"
#include "ncurses.h"
#include "../Settings.h"
#include "../Example/BorderDecorator.h"

using namespace std;

bool Blesses::Resize::sendInput(int p_key) {
    int l_width, l_height;
    getmaxyx(stdscr, l_height, l_width);
    return l_height>=Settings::getSettings()->getMinHeight()
        && l_width>=Settings::getSettings()->getMinWidth();
}

void Blesses::Resize::clean() {
    werase(m_win);
}

void Blesses::Resize::draw() {
    Blesses::State::draw();
    int l_width, l_height;
    getmaxyx(stdscr, l_height, l_width);
    mvwprintw(m_win, 1, 1,
            "Your window is too small(%dx%d), must be at least %dx%d. Resize it!",
            l_height, l_width,
            Settings::getSettings()->getMinHeight(), Settings::getSettings()->getMinWidth());
    wrefresh(m_win);
}

Blesses::Resize::Resize() {
    getmaxyx(stdscr, m_height, m_width);

    Blesses::State::m_win = newwin(m_height, m_width, 0, 0);
}

std::string Blesses::Resize::getName() {
    return "Resize State";
}
