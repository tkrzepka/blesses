//
// Created by Tomasz Rzepka on 13/06/15.
//

#pragma once
#include "State.h"
namespace Blesses {
    class Resize : public State {
    public:
        virtual std::string getName() override;

        virtual bool sendInput(int p_key) override;
        virtual void clean() override;
        virtual void draw() override;
        Resize();

    };
}