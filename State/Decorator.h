//
// Created by Tomasz Rzepka on 14/06/15.
//

#pragma once

#include <memory>
#include <ncurses.h>

namespace Blesses {
    class Decorator {

    public:
        virtual void decorate(WINDOW *p_win) = 0;
        void attach(std::shared_ptr<Blesses::Decorator> p_decorator) {
            if(m_decorator)
                m_decorator->attach(p_decorator);
            else
                m_decorator=p_decorator;
        }

        std::shared_ptr<Decorator> m_decorator;
    };
}