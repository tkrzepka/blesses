//
// Created by Tomasz Rzepka on 13/06/15.
//

#pragma once

#include <memory>
#include <curses.h>
#include <stack>
#include "State/State.h"
#include "State/StateMachine.h"

namespace Blesses {

    class Controller {
    public:
        void initialize();
        static std::shared_ptr<Controller> getController();

        static bool isRunning();
        static void start(std::shared_ptr<State>& p_state);
        static void stop();

        void acceptVisitor(std::shared_ptr<Visitor>& p_visitor);
        void getKey(int p_key);
        void draw();
        void drawAll();
        void push(std::shared_ptr<State>& p_state);
        void push(std::shared_ptr<State>&& p_state);
        ~Controller();

    private:
        Controller();

        static std::shared_ptr<Controller> m_mainController;
        static bool m_running;
        std::unique_ptr<StateMachine> m_stateMachine{std::make_unique<StateMachine>()};
    };

}