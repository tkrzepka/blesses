#include <iostream>
#include <memory>
#include <chrono>
#include <thread>
#include "Settings.h"
#include "Controller.h"
#include "Example/ResizeVisitor.h"

using namespace std;
using namespace Blesses;
#define RESIZE_KEY 0x19a

int main() {
    Settings::createSettings();
    Controller::getController()->initialize();
    std::shared_ptr<Blesses::State> initialState = make_shared<Menu>();
    Controller::getController()->start(initialState);

    std::chrono::time_point<std::chrono::system_clock> start, end;
    int frameSize = 30;
    while(Controller::isRunning()){
        start = chrono::system_clock::now();
        auto key = getch();
        Controller::getController()->getKey(key);
        if(key == RESIZE_KEY){
            std::shared_ptr<Blesses::Visitor> visitor = std::make_shared<ResizeVisitor>();
            Controller::getController()->acceptVisitor(visitor);
        }

        Controller::getController()->drawAll();
        end = chrono::system_clock::now();
        int elapsedTime = chrono::duration_cast<chrono::milliseconds>(end - start).count();
        if(elapsedTime < frameSize)
            this_thread::sleep_for(chrono::milliseconds(frameSize-elapsedTime));
    }
}
