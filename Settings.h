//
// Created by Tomasz Rzepka on 13/06/15.
//

#pragma once

#include <curses.h>
#include <memory>

namespace Blesses {
    class Settings {
    public:
        static bool createSettings(int p_visibleCursor = FALSE, bool p_nonBlockingGetCh = true, int p_escDelay = 25,
                                   int p_minHeight = 30, int p_minWidth = 100);
        int getCursorVisibility() const;
        bool isNonBLockingGetCh() const;
        int getEscDelay() const;
        int getMinHeight() const;
        int getMinWidth() const;
        static std::shared_ptr<Settings> getSettings();
        void setMinHeight(int m_minHeight);
        void setMinWidth(int m_minWidth);

    private:

        Settings(const int p_visibleCursor, const bool p_nonBLockingGetCh, const int p_escDelay, int p_minHeight,
                 int p_minWidth);

        Settings();

        static std::shared_ptr<Settings> m_settings;
        const int m_visibleCursor{FALSE};
        const bool m_nonBLockingGetCh{true};
        const int m_escDelay{25};
        int m_minHeight{30};
        int m_minWidth{100};
    };
}