//
// Created by Tomasz Rzepka on 13/06/15.
//

#include "Controller.h"
#include <ncurses.h>
#include <locale.h>
#include <iostream>
#include "Settings.h"

bool Blesses::Controller::m_running = false;

std::shared_ptr<Blesses::Controller> Blesses::Controller::m_mainController;

std::shared_ptr<Blesses::Controller> Blesses::Controller::getController() {
    if(!m_mainController) {
        auto controller= new Blesses::Controller();
        m_mainController.reset(controller);
    }
    return m_mainController;
}

Blesses::Controller::Controller() {
}

Blesses::Controller::~Controller() {
    endwin();
}

void Blesses::Controller::initialize() {

    setlocale(LC_ALL, "");
    initscr();
    cbreak();
    raw();
    keypad(stdscr, TRUE);
    noecho();
    curs_set(Settings::getSettings()->getCursorVisibility());
    nodelay(stdscr, Settings::getSettings()->isNonBLockingGetCh());
    set_escdelay(Settings::getSettings()->getEscDelay());
}

void Blesses::Controller::getKey(int p_key) {
    m_stateMachine->sendInput(p_key);
    m_stateMachine->draw();
}

void Blesses::Controller::draw(){
    m_stateMachine->draw();
}

void Blesses::Controller::drawAll(){
    m_stateMachine->drawAll();
}

bool Blesses::Controller::isRunning(){
    return m_running;
}

void Blesses::Controller::stop() {
    m_running = false;
}

void Blesses::Controller::acceptVisitor(std::shared_ptr<Visitor> &p_visitor) {
    m_stateMachine->acceptVisitor(p_visitor);
}

void Blesses::Controller::start(std::shared_ptr<State> &p_state) {
    getController()->m_stateMachine->pushState(p_state);
    getController()->m_stateMachine->initialize();
    m_running = true;
}

void Blesses::Controller::push(std::shared_ptr<State> &p_state) {
    m_stateMachine->pushState(p_state);
}

void Blesses::Controller::push(std::shared_ptr<State> &&p_state) {
    m_stateMachine->pushState(p_state);
}
