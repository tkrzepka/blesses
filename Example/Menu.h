//
// Created by Tomasz Rzepka on 14/06/15.
//

#pragma once

#include "../State/State.h"
#include "../State/IStateFactory.h"
#include <utility>
#include <string>
#include <functional>
#include <vector>

class Menu: public Blesses::State {

public:
    virtual std::string getName() override;
    virtual bool sendInput(int p_key) override;
    virtual void clean() override;
    virtual void draw() override;
    void resize();
    Menu();
    std::shared_ptr<Blesses::IStateFactory> getFactory();
private:
    int m_option{0};
    std::vector<std::pair<std::string, std::function<bool(void)>>> m_items;
};
