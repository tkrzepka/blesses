//
// Created by Tomasz Rzepka on 15/06/15.
//

#pragma once


#include "../State/Visitor.h"
#include "Menu.h"

class ResizeVisitor: public Blesses::Visitor{

    virtual void visit(std::shared_ptr<Blesses::State> &p_state) override;
    void resize(std::shared_ptr<Menu>& p_state);
};
