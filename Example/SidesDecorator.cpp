//
// Created by Tomasz Rzepka on 15/06/15.
//

#include "SidesDecorator.h"

void SidesDecorator::decorate(WINDOW *p_win) {
    int l_height, l_width;
    getmaxyx(p_win, l_height, l_width);
    for(int i=3;i<l_height-3;i+=2){
        mvwprintw(p_win, i, 4, "*");
        mvwprintw(p_win, i, l_width-5, "*");
    }
    if(m_decorator)
        m_decorator->decorate(p_win);
}
