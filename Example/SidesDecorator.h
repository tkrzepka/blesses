//
// Created by Tomasz Rzepka on 15/06/15.
//

#pragma once

#include "../State/Decorator.h"

class SidesDecorator: public Blesses::Decorator {
public:
    virtual void decorate(WINDOW *p_win) override;
};
