//
// Created by Tomasz Rzepka on 15/06/15.
//

#include "ResizeVisitor.h"

void ResizeVisitor::visit(std::shared_ptr<Blesses::State> &p_state) {
    std::shared_ptr<Menu> l_state = std::dynamic_pointer_cast<Menu>(p_state);
    if(l_state != 0)
        resize(l_state);
}

void ResizeVisitor::resize(std::shared_ptr<Menu> &p_state) {
    p_state->resize();
}
