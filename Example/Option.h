//
// Created by Tomasz Rzepka on 15/06/15.
//

#pragma once

#include "../State/State.h"

class Option: public Blesses::State {

public:
    Option(int m_id);
    virtual std::string getName() override;
    virtual bool sendInput(int p_key) override;
    virtual void clean() override;
    virtual void draw() override;
    void resize();
private:
    int m_id;
};
