//
// Created by Tomasz Rzepka on 15/06/15.
//

#pragma once

#include "../State/IStateFactory.h"
#include "../State/State.h"

class OptionsStateFactory: public Blesses::IStateFactory {
public:
    virtual std::shared_ptr<Blesses::State> createState(int i) override;
};
