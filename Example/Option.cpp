//
// Created by Tomasz Rzepka on 15/06/15.
//

#include <sstream>
#include "Option.h"
#include "BorderDecorator.h"

std::string Option::getName() {
    std::stringstream sstm;
    sstm << "Option: " << m_id;
    return sstm.str();
}

bool Option::sendInput(int p_key) {
    switch(p_key) {
        case '\n':
            return true;
        default:
            return false;
    }
}

void Option::clean() {
    werase(m_win);
}

void Option::draw() {
    Blesses::State::draw();
    std::stringstream sstm;
    sstm << "Option: " << m_id;
    auto header = sstm.str();
    mvwprintw(m_win, 2, static_cast<int>((m_width - header.length()) / 2), header.c_str());
    wrefresh(m_win);
}

Option::Option(int m_id) : m_id(m_id) {
    m_height = 5;
    m_width = 30;
    resize();
    m_decorator = std::make_shared<BorderDecorator>();
}

void Option::resize() {
    int l_width, l_height;
    getmaxyx(stdscr, l_height, l_width);
    if(m_win != nullptr)
        delwin(m_win);
    Blesses::State::m_win = newwin(m_height, m_width, (l_height-m_height)/2, (l_width-m_width)/2);
}
