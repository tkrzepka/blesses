//
// Created by Tomasz Rzepka on 14/06/15.
//

#include "Menu.h"
#include "BorderDecorator.h"
#include "SidesDecorator.h"
#include "OptionsStateFactory.h"
#include "../Controller.h"

using namespace Blesses;

std::string Menu::getName() {
    return "Menu";
}

bool Menu::sendInput(int p_key) {
    switch(p_key){
        case KEY_UP:
            if(m_option>0)
                m_option--;
            break;
        case KEY_DOWN:
            if(m_option<m_items.size())
                m_option++;
            break;
        case '\n':
            return m_items[m_option].second();
        default:
            break;
    }
    return false;
}

void Menu::clean() {
    werase(m_win);
}

void Menu::draw() {
    State::draw();
    std::string header = "++++++ Main Menu ++++++";
    mvwprintw(m_win, 3, static_cast<int>((m_width - header.length()) / 2), header.c_str());
    int i = 0;
    int startY = static_cast<int>(m_height - m_items.size()*3)/2+3;
    for(auto& it: m_items) {
        if (m_option == i) wattron(m_win, A_REVERSE);
        mvwprintw(m_win, startY+i*3, static_cast<int>((m_width - it.first.length()) / 2), it.first.c_str());
        if (m_option == i) wattroff(m_win, A_REVERSE);
        i++;
    }
    wrefresh(m_win);
}

Menu::Menu() {
    resize();
    m_stateFactory = std::make_shared<OptionsStateFactory>();
    m_items.push_back(std::make_pair(" Option 1 ",
                                     std::bind([&](std::shared_ptr<IStateFactory> p_stateFactory)
                                               {Controller::getController()->push(p_stateFactory->createState(1));
                                                   return false;},
                                               m_stateFactory)));
    m_items.push_back(std::make_pair(" Option 2 ",
                                     std::bind([&](std::shared_ptr<IStateFactory> p_stateFactory)
                                               {Controller::getController()->push(p_stateFactory->createState(2));
                                                   return false;},
                                               m_stateFactory)));
    m_items.push_back(std::make_pair(" Option 3 ",
                                     std::bind([&](std::shared_ptr<IStateFactory> p_stateFactory)
                                               {Controller::getController()->push(p_stateFactory->createState(3));
                                                   return false;},
                                               m_stateFactory)));
    m_items.push_back(std::make_pair(" Option 4 ",
                                     std::bind([&](std::shared_ptr<IStateFactory> p_stateFactory)
                                               { Controller::getController()->push(p_stateFactory->createState(4));
                                                   return false;},
                                               m_stateFactory)));
    m_items.push_back(std::make_pair("   Exit   ",[]{return true;}));
    m_decorator = std::make_shared<BorderDecorator>();
    m_decorator->attach(std::make_shared<SidesDecorator>());
}

void Menu::resize() {
    int l_width, l_height;
    getmaxyx(stdscr, l_height, l_width);
    m_height = static_cast<int>(l_height*0.8);
    m_width = static_cast<int>(l_width*0.3);
    if(m_win != nullptr)
        delwin(m_win);
    Blesses::State::m_win = newwin(m_height, m_width, (l_height-m_height)/2, (l_width-m_width)/2);
}

std::shared_ptr<Blesses::IStateFactory> Menu::getFactory() {
    return Blesses::State::m_stateFactory;
}