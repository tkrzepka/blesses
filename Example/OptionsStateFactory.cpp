//
// Created by Tomasz Rzepka on 15/06/15.
//

#include "OptionsStateFactory.h"
#include "Option.h"

std::shared_ptr<Blesses::State> OptionsStateFactory::createState(int i) {
    return std::make_shared<Option>(i);
}
