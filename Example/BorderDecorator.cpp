//
// Created by Tomasz Rzepka on 14/06/15.
//

#include "BorderDecorator.h"

void BorderDecorator::decorate(WINDOW *p_win) {
    int l_height, l_width;
    getmaxyx(p_win, l_height, l_width);
    mvwadd_wch(p_win, 0, 0, m_upLeft);
    mvwadd_wch(p_win, 0, l_width-1, m_upRight);
    mvwadd_wch(p_win, l_height-1, 0, m_downLeft);
    mvwadd_wch(p_win, l_height-1, l_width-1, m_downRight);
    for(int i=1; i<l_width-1; i++){
        mvwadd_wch(p_win, 0, i, m_upDown);
        mvwadd_wch(p_win, l_height-1, i, m_upDown);
    }
    for(int i=1; i<l_height-1; i++){
        mvwadd_wch(p_win, i, 0, m_leftRight);
        mvwadd_wch(p_win, i, l_width-1, m_leftRight);
    }
    if(m_decorator)
        m_decorator->decorate(p_win);
}
