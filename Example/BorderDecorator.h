//
// Created by Tomasz Rzepka on 14/06/15.
//

#pragma once

#include "../State/Decorator.h"

class BorderDecorator:public Blesses::Decorator {

public:
    virtual void decorate(WINDOW *p_win) override;

private:
    const cchar_t* m_upDown = WACS_HLINE;
    const cchar_t* m_leftRight = WACS_VLINE;
    const cchar_t* m_upLeft = WACS_ULCORNER;
    const cchar_t* m_upRight = WACS_URCORNER;
    const cchar_t* m_downLeft = WACS_LLCORNER;
    const cchar_t* m_downRight = WACS_LRCORNER;
};
