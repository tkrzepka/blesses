//
// Created by Tomasz Rzepka on 13/06/15.
//

#include "Settings.h"

std::shared_ptr<Blesses::Settings> Blesses::Settings::m_settings;

int Blesses::Settings::getCursorVisibility() const{
    return m_visibleCursor;
}

bool Blesses::Settings::isNonBLockingGetCh() const{
    return m_nonBLockingGetCh;
}

int Blesses::Settings::getEscDelay() const{
    return m_escDelay;
}

int Blesses::Settings::getMinHeight() const{
    return m_minHeight;
}

void Blesses::Settings::setMinHeight(int p_minHeight) {
    m_minHeight = p_minHeight;
}

int Blesses::Settings::getMinWidth() const{
    return m_minWidth;
}

void Blesses::Settings::setMinWidth(int m_minWidth) {
    Blesses::Settings::m_minWidth = m_minWidth;
}

Blesses::Settings::Settings(const int p_visibleCursor, const bool p_nonBLockingGetCh, const int p_escDelay, int p_minHeight,
                   int p_minWidth) : m_visibleCursor(p_visibleCursor), m_nonBLockingGetCh(p_nonBLockingGetCh),
                                     m_escDelay(p_escDelay), m_minHeight(p_minHeight), m_minWidth(p_minWidth) { }

Blesses::Settings::Settings() {}

std::shared_ptr<Blesses::Settings> Blesses::Settings::getSettings() {
    if(!m_settings) {
        auto settings = new Blesses::Settings();
        m_settings.reset(settings);
    }
    return m_settings;
}

bool Blesses::Settings::createSettings(int p_visibleCursor, bool p_nonBlockingGetCh, int p_escDelay,
                                     int p_minHeight, int p_minWidth) {
    if(!m_settings) {
        auto settings = new Blesses::Settings(p_visibleCursor, p_nonBlockingGetCh, p_escDelay, p_minHeight, p_minWidth);
        m_settings.reset(settings);
        return true;
    }
    else
        return false;
}